export class StationInfo{
    city: string;
    latitude: number;
    longitude: number;
    name: string
}
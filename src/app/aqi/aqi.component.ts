import {Component, Input, OnInit} from '@angular/core';
import {Station} from '../models/station';
import {ObjectKeyService} from '../services/object-key-service/object-key.service';

@Component({
  selector: 'aqi',
  templateUrl: './aqi.component.html',
  styleUrls: ['./aqi.component.css']
})
export class AqiComponent implements OnInit {

  @Input() station: Station;
  private getObjectKeys = ObjectKeyService.getObjectKeys;

  constructor() { }

  ngOnInit() {
  }

}

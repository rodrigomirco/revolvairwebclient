FROM node:latest
WORKDIR /var/www
RUN git clone https://gitlab.com/charly2857/revolvairwebclient.git && cd ./revolvairwebclient && git checkout master
WORKDIR /root/
RUN npm install -g @angular/cli@1.6.5
WORKDIR /var/www/revolvairwebclient
RUN npm install
EXPOSE 4200
CMD ["ng", "serve", "--host", "0.0.0.0"]